library(SCAN.UPC)

exampleFilePath = commandArgs()[7]
version = commandArgs()[8]
species = commandArgs()[9]
annotationDB = commandArgs()[10]

description = InstallBrainArrayPackage(exampleFilePath, version, species, annotationDB)
print(description)
