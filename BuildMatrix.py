import os, sys, glob, gzip

inDirPath = sys.argv[1]
metaFilePath = sys.argv[2]
gseFilePath = sys.argv[3]
queryBatch = sys.argv[4]
outFilePath = sys.argv[5]

metaDict = {}
batches = set()
metaFile = open(metaFilePath)

metaHeaderItems = metaFile.readline().rstrip().split("\t")
cellLineNameIndex = metaHeaderItems.index("Cell line primary name")
arrayNameIndex = metaHeaderItems.index("Expression arrays")
for line in metaFile:
    lineItems = line.rstrip().split("\t")
    arrayName = lineItems[arrayNameIndex]

    if arrayName != "":
        metaDict[lineItems[cellLineNameIndex]] = arrayName
        batch = arrayName.split("_")[0]
        batches.add(batch)

metaFile.close()

#print "Available batches are: %s" % ",".join(sorted(list(batches)))

for line in file(gseFilePath):
    lineItems = line.rstrip().split("\t")

    if lineItems[0] == "!Sample_geo_accession":
        geoAccessionItems = [x.replace("\"", "") for x in lineItems[1:]]
    if lineItems[0] == "!Sample_title":
        titleItems = [x.replace("\"", "") for x in lineItems[1:]]

gseDict = {}
for i in range(len(geoAccessionItems)):
    gseDict[geoAccessionItems[i]] = titleItems[i]

dataDict = {}
sampleNames = []

inFilePaths = glob.glob(inDirPath + "/*")

inFile = gzip.open(inFilePaths[0])
inFile.readline()
for line in inFile:
    lineItems = line.rstrip().split("\t")
    dataDict[lineItems[0]] = {}
inFile.close()

for inFilePath in inFilePaths:
    sampleID = os.path.basename(inFilePath).replace(".gz", "")
    cellLineName = gseDict[sampleID]

    if cellLineName not in metaDict:
        continue

    sampleName = metaDict[cellLineName]
    batch = sampleName.split("_")[0]

    if queryBatch != "*" and batch != queryBatch:
        continue

    sampleNames.append(cellLineName)

    inFile = gzip.open(inFilePath)
    inFile.readline()
    for line in inFile:
        lineItems = line.rstrip().split("\t")
        gene = lineItems[0]
        value = lineItems[1]

        dataDict[gene][cellLineName] = value
    inFile.close()

if len(sampleNames) == 0:
    print "No samples for the %s batch." % queryBatch
    exit(0)

features = sorted(dataDict.keys())
sampleNames = sorted(sampleNames)

#features = features[:10]

outFile = open(outFilePath, 'w')
outFile.write("\t".join([""] + features) + "\n")
for sampleName in sampleNames:
    outFile.write("\t".join([sampleName] + [dataDict[feature][sampleName] for feature in features]) + "\n")
outFile.close()
