import os, sys
from utilities import *

inFilePath = sys.argv[1]

data = readMatrixFromFile(inFilePath)

if len(data[0]) == len(data[1]) - 1:
    data[0].insert(0, "")

writeMatrixToFile(data, inFilePath)
